<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$auction}}</title>
</head>
<body>
	<h1>Veiling: {{$auction->title}}</h1>
	<h2>{{$sender->name}} heeft een vraag over uw veiling:</h2>

	<p>{{$content}}</p>
</body>
</html>


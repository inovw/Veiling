@extends('master')

@section('content')
    <div class="faq-wrapper">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="title">
                    <h1>Find what you're looking for?</h1>
                </div>
                @if(count($faqs) != 0)
                    <div class="questions">
                        @foreach($faqs as $faq)
                            <div class="col-lg-3"><a href="#">{{$faq->question}}?</a></div>
                        @endforeach

                    </div>
                @else
                    <h3>Sorry nothing found...</h3>
                @endif
                <div class="search">
                    <form method="GET" action="/FAQ/search">
                        <input placeholder="{{trans('messages.search')}}" name="query" type="text">
                    </form>
                </div>
            </div>
        </div>
        <div class="row faq">
            <div class="col-lg-8 col-lg-offset-2">
                <table>
                    @foreach($faqs as $faq)
                        <tr class="question">
                            <td class="letter"><h4>Q</h4></td>
                            <td><h4>{{$faq->question}}?</h4></td>
                        </tr>
                        <tr>
                            <td><h4>A</h4></td>
                            <td>{{$faq->answer}}</td>
                        </tr>
                    @endforeach
                </table>


            </div>
        </div>
    </div>
@endsection
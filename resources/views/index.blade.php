@extends('master')

@section('content')

    <div id="work" class="row">
        <div class="col-lg-12">
            <h2 class="text-center">{{trans('messages.work')}}</h2>
        </div>
        <div class="row">
            <div class="col-lg-2 col-lg-offset-3">
                <img class="center-block" src="{{asset('img/signup.jpg')}}" alt="Sign up">
                <h3 class="text-center">{{trans('messages.signup')}}</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu tempus tortor, ut suscipit tellus.
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
            </div>
            <div class="col-lg-2">
                <img class="center-block" src="{{asset('img/deals.jpg')}}" alt="Deals">
                <h3 class="text-center">{{trans('messages.deals')}}</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu tempus tortor, ut suscipit tellus.
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
            </div>
            <div class="col-lg-2">
                <img class="center-block" src="{{asset('img/happy.jpg')}}" alt="Happy">
                <h3 class="text-center">{{trans('messages.happy')}}</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu tempus tortor, ut suscipit tellus.
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
            </div>
        </div>
    </div>

    <div class="row" id="popular">
        <div class="col-lg-6 col-lg-offset-3">
            <h2 id="pophead" class="text-center">{{trans('messages.mostpopular')}}</h2>
            <span id="expand" class="glyphicon glyphicon-down"></span>
        </div>
        <div class="row popimg">
            <div class="col-lg-offset-2 col-lg-2">
                <div class="row">
                    <a class="popular" href="/art/{{$popular[1]->id}}">
                        <div class="img-small img col-lg-12"
                             style="background-image: url('../img/{{$popular[1]->artwork()->first()->image}}')">
                            <div class="after"><span class="icon-small glyphicon glyphicon-search"></span></div>
                        </div>
                    </a>
                    <a class="popular" href="/art/{{$popular[2]->id}}">
                        <div class="img-small img col-lg-12"
                             style="background-image: url('../img/{{$popular[2]->artwork()->first()->image}}')">
                            <div class="after"><span class="icon-small glyphicon glyphicon-search"></span></div>
                        </div>
                    </a>
                </div>


            </div>
            <a class="popular" href="/art/{{$popular[0]->id}}">
                <div class="img-big img col-lg-6"
                     style="background-image: url('../img/{{$popular[0]->artwork()->first()->image}}')">
                    <div class="after"><span class="icon-big glyphicon glyphicon-search"></span></div>
                </div>
            </a>
        </div>
        <div class="row popular-carousel">

        </div>
    </div>

@endsection
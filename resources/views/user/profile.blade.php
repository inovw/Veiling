@extends('master')
@section('content')
    <div class="row profile">
        <div class="col-lg-offset-2 col-lg-8">
            <h1>Profile</h1>
            <div class="content">
                <h3>{{$user->name}}</h3>
                <div class="upper">
                    <p><span class="glyphicon glyphicon-envelope"></span> <a
                                href="mailto:{{$user->email}}">{{$user->email}}</a></p>
                    <p><span class="glyphicon glyphicon-earphone"></span> {{$user->phone}}</p>
                </div>
                <div class="lower">
                    <p>{{$user->address}}</p>
                    <p>{{$user->zip}} {{$user->city}}</p>
                </div>
            </div>
        </div>
        <div class="auctions col-lg-offset-2 col-lg-8">
            <h2>Active auctions</h2>
            <div class="row">
                @if(count($activeAuctions != 0))
                    @foreach($activeAuctions as $index => $auction)
                        <div class="col-lg-3">
                            <a href="/art/{{$auction->id}}">
                                <div class="image" style="background-image: url('../img/{{$auction->artwork->image}}')">
                                    <div class="after"><span class="glyphicon glyphicon-search"></span></div>
                                </div>
                            </a>
                            <p>{{$auction->artwork->year}}, {{$auction->artwork->artist->name}}</p>
                            <h3>{{$auction->title}}</h3>
                            <h4>€ {{$auction->price}}</h4>
                            <div class="bottom">
                                <p class="time">{{$timediff[$index]}}</p>
                                <div class="button">
                                    <a href="/art/{{$auction->id}}">Visit auction <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center center-block no-results">
                        <h1>This user has no active auctions...</h1>
                    </div>
                @endif
            </div>
        </div>

    </div>
@endsection


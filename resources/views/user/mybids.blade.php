@extends('master')
@section('content')
    <div class="bids-wrapper">
        <div class="row">
            <div class="col-lg-2 col-lg-offset-2">
                <h2>{{trans('messages.mybids')}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <table class="table bids table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Auction</th>
                        <th>Your Bid</th>
                        <th>End date</th>
                        <th>Remaining time</th>
                    </tr>
                    </thead>
                    @foreach($bids as $bid)
                        <tr>
                            <td class="image" style="background-image: url('../img/kunst.jpg')"></td>
                            <td>{{$bid->auction->title}}
                            </td>
                            <td>€ {{$bid->price}}</td>
                            <td>{{date('F d, Y', strtotime($bid->auction->end))}}</td>
                            <td>{{\App\Classes\timecalc::calculateOne($bid->auction)}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
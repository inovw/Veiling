## Installation for C9

### Clone the repository from Github**
Choose the PHP, Apache & MySQL Template
```bash
$ git clone https://gitlab.com/Snodvod/Veiling.git
```

### Install packages**
```bash
$ composer install
```

### Setup Apache**
```bash
$ sudo nano /etc/apache2/sites-enabled/001-cloud9.conf
```
Set DocumentRoot & Directory to '/home/ubuntu/workspace/public'

### Optional: install PhpMyAdmin**
```bash
$ mysql-ctl start
$ phpmyadmin-ctl install
```

###Optional: adjust the env file**
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=h1aznyW7rSVorary6uLVoSm16xhJUTSz

DB_HOST=127.0.0.1
DB_DATABASE=c9
DB_USERNAME= YOURUSERNAMEHERE
DB_PASSWORD= PASSWORDHERE

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=mailgun
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=587

MAILGUN_DOMAIN=sandbox48b5f6d27f7e47d1bf3311d177974d39.mailgun.org
MAILGUN_SECRET=key-5b06bd9f250f181781cbd4971fd29f44

FB_CLIENT_ID= 'insert id of facebook'
FB_SECRET_ID= 'insert secret of facebook'
```

**Migrate and seed database**
```bash
$ php artisan migrate:refresh --seed
```

**Add cronjob**
```bash
$ crontab -e
```
Add this to the bottom of the file
```
* * * * * php /home/ubuntu/workspace/artisan schedule:run >> /dev/null 2>&1
```
```bash
$ sudo cron start
```

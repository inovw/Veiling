<?php

use Illuminate\Database\Seeder;

class FaqTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faq_tag')->insert([
            'faq_id' => 1,
            'tag_id' => 1
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 2,
            'tag_id' => 2
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 3,
            'tag_id' => 3
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 4,
            'tag_id' => 4
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 5,
            'tag_id' => 5
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 6,
            'tag_id' => 6
        ]);

        DB::table('faq_tag')->insert([
            'faq_id' => 7,
            'tag_id' => 6
        ]);
    }
}

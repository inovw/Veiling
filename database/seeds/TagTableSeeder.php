<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'name' => 'bids',
        ]);

        DB::table('tags')->insert([
            'name' => 'sell',
        ]);

        DB::table('tags')->insert([
            'name' => 'buy',
        ]);

        DB::table('tags')->insert([
            'name' => 'register',
        ]);

        DB::table('tags')->insert([
            'name' => 'questions',
        ]);

        DB::table('tags')->insert([
            'name' => 'watchlist',
        ]);
    }
}

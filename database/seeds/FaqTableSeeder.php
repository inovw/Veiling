<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->insert([
            'question' => 'How to bid',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'How to make money to buy everything',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'How do I buy an item',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'How to register',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'How to ask a question about an item',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'What is a watchlist',
            'answer' => 'Lorem Ipsum'
        ]);

        DB::table('faqs')->insert([
            'question' => 'How to use a watchlist',
            'answer' => 'Lorem Ipsum'
        ]);
    }
}

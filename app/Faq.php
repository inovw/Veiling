<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';

    protected $fillable = [
        'question',
        'answer'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}

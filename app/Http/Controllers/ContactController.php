<?php

namespace App\Http\Controllers;

use App\Auction;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    /**
     * Show contact, when with an id the auction gets prefilled
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = '')
    {
        $auctions = Auction::All();
        if ($id != '') {
            $selected = $auctions[$id - 1];
            return view('contact', ['auctions' => $auctions, 'selected' => $selected]);
        } else {
            return view('contact', ['auctions' => $auctions]);
        }
    }

    public function send(Request $request)
    {
        $auction = Auction::find($request->auction);
        $user = $auction->owner()->first();
        $sender = Auth::user();
        $subject = $request->subject;
        Mail::send('emails.contact', ['sender' => $sender, 'user' => $user, 'content' => $request->message, 'auction' => $auction], function ($m) use ($user, $subject) {
            $m->from('info@landoretti', 'Landoretti');
            $m->to($user->email, $user->name)->subject($subject);
        });

        return redirect('/art/' . $request->auction);
    }
}

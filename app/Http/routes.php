<?php
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect']
], function() {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    

// Auction Routes
    Route::post('/search', 'ArtController@search');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('auctions', 'UserController@auctions');
        Route::get('bids', 'UserController@bids');
        Route::get('auctions/new', 'UserController@create');
        Route::get('/profile', 'ProfileController@showProfile');
        Route::post('auctions/store', 'UserController@store');
        Route::post('bid', 'BidController@bid');
        Route::post('/art/buy', 'BidController@buy');
        Route::get('/contact/{id?}', 'ContactController@index');
        Route::post('/contact', 'ContactController@send');
        Route::get('watchlist/add/{id}', 'ArtController@addToWatchlist');
        Route::get('watch/delete/{id}', 'ArtController@deleteFromWatchlist');
    });

    Route::get('FAQ', 'FaqController@index');
    Route::get('FAQ/search', 'FaqController@search');

    Route::get('art/{id}', 'ArtController@show');
    Route::get('art', 'ArtController@index');

    Route::get('art/sort/{param}', 'ArtController@index');
    Route::get('art/filter/{param1}/{param2}', 'ArtController@index');

// Auth Routes
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

// Facebook Login...
    Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');
});
